<!doctype html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Kartka Świąteczna</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

<div id="body-main" class="body-main">
	<div id="christmas-box-title" class="christmas-box-title">
		<h1>
			<span>W</span>
			<span>e</span>
			<span>s</span>
			<span>o</span>
			<span>ł</span>
			<span>y</span>
			<span>c</span>
			<span>h</span>
			<span> </span>
			<span> </span>
			<span> </span>
			<span>Ś</span>
			<span>w</span>
			<span>i</span>
			<span>ą</span>
			<span>t</span>
		</h1>
	</div>
</div>
<div class="audio-control">
	<div class="song-christmas-box">
		<audio id="christmas-song" src="assets/christmas-song.mp3" controls autoplay loop></audio>
	</div>
</div>

<script src="js/particles.js"></script>
<script src="js/app.js"></script>
</body>
</html>
